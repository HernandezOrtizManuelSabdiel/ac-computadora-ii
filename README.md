# Computadoras II

Se presentaran tres mapas conceptuales 


## Computadoras Electrónicas

```plantuml
@startmindmap
*[#lightcoral] COMPUTADORA\nELECTRÓNICA
**_ precedentes 
***[#lightgreen] Las "computadoras de proposito especifico" \nfuncionan para una sola cosa 
****_ lo que \ngenero
***** Un gran exito en el gobierno y los negocios
**** Tras el crecimiento poblacional y las guerras se generaron ciertos eventos 
*****_ como
****** el comercio mundial y las redes de transito \nse interconectaron como nunca antes
****** la ciencia y la ingenieria alcanzaron nuevos \nlimites
****** Surgio la idea de explorar otros planetas 
****** Ocurrio una explosion en la complejidad en la \nburocracia 
*******_ generando
******** un aumento en la necesidad de \nautomatizacion y de computacion
**** en poco tiempo los dispositivos \npasaron de ser un gabinete a ocupar grandes\n cuartos
*****_ llegando asi a 
****** Generar una innovacion
**[#lightblue] Harvard Mark I
***[#lightgreen] Construida por\nIBM en 1944
**** Computadora electromecánica
***** 765,000 componentes
***** 80 km de cable
***** Eje de 15 metros
***** Motor de 5 caballos de fuerza
**** Alguno de sus tantos usos fue \npara los calculos y simulaciones para\nel proyecto Manhattan
****_ La piedra angular de \nestas bestias electromecanicas era
***** RELÉ
****** Interruptor mecánico\ncontrolado eléctricamente
******* Esta compuesto por
******** Una bobina
********* No es mas que un alambre de cobre\naislado y enrollado
******** Un brazo de hierro
******** Contactos
******* Su funcionamiento \nse basa en
******** Cuando una corriente electrica atravieza la bobina\nse genera un campo magnetico, el campo\n atrae al brazo metalico y luego\n como este brazo esta conectado a los contactos\n los une, cerrando el circuito.
******* Un buen RELÉ tenia la capacidad\nde cambiar de estados unas\n50 veces por segundo
********_ Con esto lograba realizar ...
********* 3 sumas o restas por segundo
********* Multiplicación en 6s
********* División en 15s
********* Operaciones complejas\no funciones trigonometricas\nen minutos
******* Posibilidad\nde avería
******** Harvard Mark I contaba con 3,500 relés
********* Por estadística fallaba un relé al día
****** Debido a las grandes desventajas\nnace en 1904 "valvula termoionica"\npor Jhon Ambrose Fleming
******* Esta formada por
******** Un filamento 
******** Dos electrodos (catodo y anotodo) dentro de un bulbo de cristal sellado
********* Se creo y agrego un tercer electrodo llamado\nelectrodo de control
********** Con este se permite el flujo o detiene la corriente respectivamente
********** Tenian la capasidad de cambiar de estado\nmiles de veces por segundo
******* Funcionamiento 
******** Cuando el filamento se enciende y se\ncalienta permite el flujo de electrones\ndesde el anodo hacia el catodo
**** Se crea el término bug para referirse a errores en las computadoras, por encontrar una polilla muerta en un relé
***** Por el mismo calor generado las polillas se metian
**[#lightblue] Era de las computadoras\nelectrónicas
***[#lightgreen] Colossus\nMark I
**** Diseñada por Tommy\nFlowers en 1943
***** es conocida como por
****** Contar con 1,600 tubos de vacío
****** La contruccion de construyeron 10 Colossus para ayudar en la decodificacion
****** Ser la primer computadora electronica programable
****** La programacion de la misma se realizaba mediante la conexión de cientos de cables en un tablero
****** Ayudar a decodificar las comunicaciones nazis
*****_ Fue el primero\nque utilizo a gran\nescala los
****** TUBOS DE VACÍO
******* Logran la misma funcionalidad\nque los relés
******** Cambian de estado miles de veces por segundo
******** Sin partes móviles, los relés se dañan menos con el uso continuo
******** Su principal desventaja es que son frágiles y se pueden quemar, como un foco
***[#lightgreen] ENIAC
**** Sus siglas se basan en:\nCalculadora\nIntegradora Numérica\nElectrónica
***** Construida en 1946 en la universidad de pensilvania
***** Diseñada por John Mauchly y J. Presper en 1946
***** "Primer computadora electrónica programable de propósito general"
****** ENIAC podía realizar 5,000 sumas y restas de 10 dígitos por segundo
***** Operó durante 10 años
****** Debido a las constantes fallas solo era operacional \ndurante la mitad del dia
***_ en 1947 surge el
****[#lightgreen] TRANSISTOR
***** Desarrollado por John Bardeen, Walter Brattain\ny William Shockley en los laboratorios Bell
****** A grandes rasgos es un interruptor eléctrico
******* Hechos de silicio
******** Gracias al doping se le agregan otros elementos 
********* Esta hecho por 3 capas de este tipo de silicio\nen forma de sandwich
********** Cada capa representa un conector
*********** Se les conoce como conector, emisor y base
******* Velocidad de 10,000 Hz
******* Más resistentes y pequeños
****** Al dia de hoy
******* Tiene un tamaño menor a 50 nm
******* Son resistentes y veloces
****** Se construyo la IBM 608 en 1957
******* Primera computadora disponible\ncomercialmente basada en transistores
******** podía realizar
********* 4,500 sumas por segundo
********* 80 divisiones o multiplica-\nciones por segundo

@endmindmap
```

## Arquitectura Von Neumann y Arquitectura Harvard

```plantuml
@startmindmap
*[#lightcoral] Arquitectura Von Neumann\ny Arquitectura Harvard
**[#lightblue] Ley de Moore
*** Establece que
**** La velocidad del procesador de las\ncomputadoras se duplica cada 12 meses
*****_ en
****** Electronica
******* El número de transistores por cada chip se duplica cada año
******* El costo del chip permanece sin cambios
******* Cada 18 meses se duplica la potencia de cálculo sin modificar el costo
****** Performance
******* Se incrementa la velocidad del procesador
******* Se incrementar la capacidad de la memoria
******* La velocidad de memoria corre siempre por detras de la velocidad del procesador
**[#lightblue] Funcionamiento básico\nde las computadoras
*** Nivel funcional
**** ANTES
***** Había sistemas cableados
***** Programación mediante el Hardware
****** Cuando se requiere realizar otra\ntarea se debe cambiar el hardware
*****_ su flujo era
****** Ingreso de datos
****** Secuencia de funciones aritméticas/lógicas
****** Resultados
**** AHORA
***** La programación mediante Software
****** En cada paso se realiza alguna\noperacion sobre datos
*****_ su flujo es
****** Ingreso de datos
****** Secuencia de funciones aritméticas/lógicas
*******_ Se apoya de
******** Señales de control
*********_ que a su ves se apoya de
********** Interprete de instrucciones
****** Resultados
**[#lightblue] Esta compuesta por
***[#lightgreen] Arquitectura\nVon Neumann
**** Arquitectura moderna de\nlos ordenadores actuales
***** Compuesta por
****** PARTES
******* Existen 3 partes fundamentales
******** Procesador o CPU\n(unidad central de procesamiento)
*********_ contiene
********** Una Unidad de Control
********** Una Unidad Aritmética Lógica
********** Registros
***********_ permiten
************ contador de programas
************ registro de instruccion
************ registro de direccion de memoria
************ registro de buffer de memoria
************ registro de direcciones de entrada/salida
************ registro de buffer de entrada/salida
******** Memoria principal de instrucciones y datos
******** Módulo de Entrada/Salida
********_ cuando se requierae\njecutar algo
********* lo vamos a tener en un modulo de\nEntrada/salida
********** tendremos que buscarlo a memoria principal
***********_ y despues
************ la CPU va a ir trabajando
***** Su modelo
****** Arquitectura descrita por el matemático\ny fisico Von Neumann en 1945
******* Los datos y programas se almacenan en una única memoria de lectura/escritura
******* Los contenidos de esta memoria se acceden indicando su posición
******* Ejecución en secuencia 
******** A menos que se indique lo contrario
******* Representacion binaria
***** Que contiene
****** Describe una arquitectura\npara un computador digital
*******_ sus partes son
******** CPU, Memoria principal\ny sistema de entrada/salida
***** Debido a este modelo
****** Surge el concepto de programa almacenado
****** La separación de la memoria y la CPU genera el problema llamado\nNeumann bottleneck o cuello de botella
******* Esto es debido a la cantidad de datos que pasa entre estos\ndos elementos difiere mucho en tiempo con\n las velocidades de ellos(throughput)
******* La CPU puede\npermanecer osciosa
***** Modelo de bus
****** El bus es un dispositivo en comun entre\ndos o mas dispositivos
******* Si dos dispositivos transmiten el mismo\ntipo de señal genera consecuencias
********_ que son
********* Distorsionarse entre si
********* Perder informacion
********_ por ello
********* Existe un arbitraje para decidir\nque dispositivo hace uso del bus
****** Cada linea puede transmitir señales\nque representan unos y ceros
******* Generando que se una señal por unidad de tiempo
******** Si se decea transmitir un byte\nse deben mandar 8 señales
********* Tardando 8 unidades de tiempo
****** Es un refinamiento del modelo Von Neuman
****** Su proposito es reducir la cantidad\nde conexiones entre la CPU y sus sistemas
****** La comunicacion entre componentes\nse maneja por un camino compartido llamado bus
*******_ esta se divide en 
******** Bus de datos
******** Bus de direcciones
******** Bus de control
***** Instrucciones
****** La función de una computadora es la ejecución\nde programas localizados en memoria
******* El CPU se encarga de ejecutar las\ninstrucciones a través de un ciclo\ndenominado ciclo de instrucción
******** para ello se emplean lenguajes
********* Lenguajes de bajo nivel (ensamblador)
********* Lenguajes de alto nivel (Java o Python)
******** Por ejemplo 3 billones de operaciones\npor segundo para una CPU con 3GHz
***** Ciclos
****** De ejecución
******* Fetch de instrucción
******** UC obtiene la próxima instrucción de memoria y\ndejando la informacion en el registro IR
******* Decodificación de la instrucción
******** La instruccion es decodificada a un lenguaje\nque entiende la ALU
******* Fetch de operandos
******** Obtiene de memoria los operandos requeridos\npor la instruccion
******* Escritura de operandos
******** ALU ejecuta y deja los resultados en registro\n o en memoria
******* Ejecución de la instrucción
******** ALU ejecuta y deja los resultados en registro\n o en memoria
****** De una instrucción
******* Cálculo de la dirección de la instrucción
******* Captación de la instrucción
******* Decodificación de la operación
******* Cálculo de la dirección del operando (dato)
******** Captación del operando
******* Operación con el dato
******* Cálculo de la dirección del operando
******** Almacenamiento del operando
***[#lightgreen] Arquitectura Harvard
**** Se refiere a las arquitecturas de computadoras\nque utilizaban dispositivos de almacenamiento fisicamente separados,\npara instrucciones y para datos
*****_ contiene
****** PARTES\nFUNDAMENTALES
******* Procesador o CPU
********_ contiene
********* Una Unidad de Control
********* Una Unidad Aritmética Lógica
******* Memoria
******** De instrucciones
********* Es la memoria donde se almacenan las instrucciones del programa,\n que deben ejecutar el microcontrolador utilizando memorias no volátiles\n(ROM, PROM, EPROM, EEPROM o flash)
******** De datos
********* Almacena los datos utilizados por los programas, habitualmente usando memoria\nSRAM, o si es necesario almacenar datos permanentemente, EEPROM o flash
******* Sistema de Entrada/Salida
******* Buses para cada memoria
******** De control
******** De dirección de instrucciones o de datos
******** De instrucciones o datos
***** USO PRINCIPAL
****** Fabricar memorias mucho más\nrápidas tiene un precio muy costoso, su solucion es proporcionar\n una pequeña cantidad de memoria conocida como memoria cache
****** Solucion
******* Las instrucciones y los datos se almacenan en\ncaches separadas para obtener un mejor rendimiento
******** Se suele utilizar en PICs y Microcontroladores\nutilizados en electrodométicos, procesamiento\nde audio y video
******** Funciona mejor cuando la frecuencia de lectura de\ninstrucciones y datos es aproximadamente la misma

@endmindmap
```
## Basura Electrónica

```plantuml
@startmindmap
*[#lightcoral] BASURA ELECTRÓNICA

**[#lightblue] Concepto
*** Son todos los aparatos electricos que han concluido con su vida util
*** Tambien son conocidos como RAEE (Residuos de aparatos eletronicos y electricos)

**[#lightblue] TESORO EN LA BASURA
*** Se pueden extrar diversos materiales de valor de los residuos electrónicos
****_ como por ejemplo
***** Estaño 
***** Oro 
***** Cobre 
***** Plata 
***** Cobalto 
***** Acero
*** Es necesario reciclarlos de forma segura

**[#lightblue] Malos usos
*** Si se tiene un mal manejo de los desechos, se puede obtener\ninformacion mediante los discos duros o celulares
**** Robo de identidad, falsificacion
**** Solucion  
***** Cuando se deseche este tipo de dispositivos\n borrar todo tipo de informacion personal

**[#lightblue] RECICLAJE
*** Proceso de reciclaje
**** Recoleccion 
**** Seleccion segun la categoria
**** Desarmado y clasificacion
**** Reintroduccion (reutilizacion)

*** Reciclaje seguro
**** Se realiza en plantas de reciclaje
**** Es muy costoso reciclar eletronicos de forma segura, pero existen\nmuchas medidas para no generar un gran impacto ambiental
**** Proceso complicado

*** Reciclaje clandestino
**** Totalmente ilegal
**** No esta regularizado
**** Se esporta clandestinamente a paises como mexico, china, india.

*** Plantas de reciclaje

**[#lightblue] DATOS
*** Muchos de los desechos electrónicos contienen\ninformación de valor de sus anteriores dueños
**** La información personal o de empresas puede\nutilizarse para muchos fines criminales
***** Existen equipos para borrar los datos de discos duros o para\ndestruirlos, de esta manera la información queda a salvo

**[#lightblue] PROBLEMA\nAMBIENTAL
*** El creciente ritmo del desarrollo de nuevas\ntecnologías y cambio tecnológico generan\ncada vez más desechos electrónicos
**** Debido a la corta vida útil de los aparatos electrónicos
****_ estos son los llamados
*****[#lightblue] RAEE
****** Residuos de Aparatos\nEléctricos y Electrónicos
******* Deben ser tratados adecua-\ndamente para su reciclaje
******** Con estos se pueden\ngenerar materias\nprimas como
********* Plásticos
********* Vidrios
********* Electrónica
********* Metales
**** En México se genera un promedio de 300,000 toneladas de residuos electrónicos

**[#lightblue] PLANTAS DE\nRECICLAJE
*** Instalación en donde se procesan diferentes materiales, en este\ncaso, residuos electrónicos, para poder ser reutilizados
****_ algunos ejemplos de este tipo de plantas
***** e-end
******_ ubicado en
******* Estados Unidos
***** Perú Green Recycling
******_ ubicado en
******* Perú
***** Techemet
******_ ubicado en
******* México
***** Servicios Ecológicos
******_ ubicado en
******* Costa Rica
***** Remsa
******_ ubicado en
******* México

** Pueden ser\ndesechos como 
*** Computadoras, celulares, discos duros, audífonos ratones y teclados,\nconsolas de videojuegos, proyectores, teléfonos, etc. En resumen cualquier\ndispositivo que ha concluido con su vida util.

@endmindmap
```

